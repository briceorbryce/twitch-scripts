# Twitch Chatters Script

Simple script to view the Twitch accounts that have connected to a given Twitch chat. 

It doesn't use argparse since there is only one condition: if you input a Twitch account or not (it does try to show a help message with some nice formatting).

In order to send Twitch required HTTP headers, the script uses urllib2.Request to build the headers. It then uses urllib2's urlopen, json loads, and dumps to format the output to an easily readable format.

The file, "oauth_briceorbryce" contains a variable called client_id which is a HTTP header required by Twitch to hit their api.
