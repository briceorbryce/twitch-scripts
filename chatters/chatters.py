#! /usr/bin/python

# Client ID is from briceortest
# General API requests application
# Scope is None
import urllib2, json, sys

from oauth_briceorbryce import client_id

if len(sys.argv) == 2:
  url = "https://tmi.twitch.tv/group/user/" + sys.argv[1]  + "/chatters"
else:
  print "Usage: " + sys.argv[0], '\033[4m' + "username" + '\033[0m'
  exit()

req = urllib2.Request(url)
req.add_header("Client-ID", client_id)
req.add_header("Accept", "application/vnd.twitchtv.v3+json")
response = urllib2.urlopen(req)

data = json.loads(response.read())
print json.dumps(data, indent=2)
