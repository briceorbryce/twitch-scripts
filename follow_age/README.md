# Twitch Follow Age Script

Simple script to view the follow age of one Twitch account to another. By default, the script computes the follow age of briceorbryce to a given Twitch account.

It uses argparse since I wanted to experiment around with it.

The file, "oauth_briceorbryce" contains a variable called client_id which is a HTTP header required by Twitch to hit their api.

In order to send Twitch required HTTP headers, the script uses urllib2.Request to build the headers. It then uses urllib2's urlopen and json loads to get the key, 'created_at' value.

Finally, the script formats the string to a proper format that datetime.strptime can understand and computes the difference between the time the script is run and the time Twitch's API reports a Twitch user has followed another user.
