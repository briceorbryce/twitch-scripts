#! /usr/bin/python

# Client ID is from briceortest
# General API requests application
import urllib2, json, sys, re
from datetime import datetime
from pprint import pprint
import argparse

from oauth_briceorbryce import client_id

parser = argparse.ArgumentParser(description="Prints the follow age of a channel")
parser.add_argument("channel", help="Channel to check.")
parser.add_argument("-u", "--user", help="User to check.", default="briceorbryce")

args = vars(parser.parse_args())
channel = args["channel"]
user = args["user"]
url = "https://api.twitch.tv/kraken/users/" + user + "/follows/channels/" + channel

req = urllib2.Request(url)
req.add_header("Client-ID", client_id)
req.add_header("Accept", "application/vnd.twitchtv.v3+json")
try:
  response = urllib2.urlopen(req)
except urllib2.HTTPError as e:
  print e
  if len(sys.argv) == 2:
    print "briceorbryce is not following " + str(sys.argv[1]) + "."
  else:
    print str(user) + " is not following " + str(channel) + "."
  exit()

data = json.loads(response.read())
start_follow = re.compile("[0-9]*-[0-9]*-[0-9]*.*[0-9]*:[0-9]*:[0-9]*")

updated_at = data["created_at"]

diff = re.search(start_follow, updated_at)
diff = re.sub('T', ' ', diff.group())
t1 = datetime.strptime(diff, "%Y-%m-%d %H:%M:%S")
t2 = datetime.utcnow()
diff = t2 - t1

years = diff.days/365
calc_months = diff.days - years*365
months = calc_months/30.42
weeks_leftover = calc_months % 30.42

weeks = weeks_leftover / 7
days = weeks_leftover % 7
hours = diff.seconds / 3600
minutes = (diff.seconds -  hours * 3600 )/ 60

print "Start date: " + str(updated_at)
print "Follow age: " + str(diff)
print "Years: " + str(int(years))
print "Months: " + str(int(months))
print "Weeks: " + str(int(weeks))
print "Days: " + str(int(days))
