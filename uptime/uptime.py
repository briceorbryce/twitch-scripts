#! /usr/bin/python

# Client ID is from briceortest
# General API requests application
import urllib2, json, re, sys
from datetime import datetime
import calendar

from oauth_briceorbryce import client_id

if len(sys.argv) > 1:
	url = "https://api.twitch.tv/kraken/streams/" + sys.argv[1]
else:
	print sys.argv[0], '\033[4m' + "stream name" + '\033[0m'
	exit()

req = urllib2.Request(url)
req.add_header("Client-ID", client_id)
req.add_header("Accept", "application/vnd.twitchtv.v3+json")
response = urllib2.urlopen(req)
data = json.loads(response.read())

try:
	created_at = str(data["stream"]["created_at"])
except TypeError:
	print sys.argv[1] + " is not streaming at the moment."
	exit()


get_start = re.compile("[0-9]*-[0-9]*-[0-9]*.*[0-9]*:[0-9]*:[0-9]*")
foo = re.search(get_start, created_at) # get_created_at
foo = re.sub('T', ' ', foo.group())

t1 = datetime.strptime(foo, "%Y-%m-%d %H:%M:%S")
mytime = calendar.timegm(t1.timetuple())
t2 = datetime.utcnow()

diff = t2 - t1
days = diff.days
hours = diff.seconds / 3600
minutes = (diff.seconds-hours * 3600) / 60
seconds = diff.seconds - hours * 3600 - minutes * 60

print "Started at: " + str(datetime.fromtimestamp(mytime)) + " HST"
print "Created_at: " + str(t1)
print "Days: " + str(days)
print "Hours: " + str(hours)
print "Minutes: " + str(minutes)
print "Seconds: " + str(seconds)
