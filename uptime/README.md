# Twitch Uptime Script

Simple script to view the time Twitch reports a streamer has been streaming.

It doesn't use argparse since there is only one condition: if you input a Twitch account or not (it does try to show a help message with some nice formatting).
In order to send Twitch required HTTP headers, the script uses urllib2.Request to build the headers. It then uses urllib2's urlopen, json loads, and dumps to format the output.

From the output, it checks the 'stream' and the 'created_at' keys to get the value in UTC time. If the 'stream' key is not in the output then the given account either is not streaming or doesn't exist.

Next, it does some formatting and conversions to HST and takes the current time in UTC. That way it can compute the difference between the time that Twitch reports the streamer started streaming and current time.

The file, "oauth_briceorbryce" contains a variable called client_id which is a HTTP header required by Twitch to hit their api.
