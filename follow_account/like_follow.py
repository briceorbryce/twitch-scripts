#! /usr/bin/python

# Client ID is from briceortest
# follow app for briceorbryce application
# Scope is user_follows_edit
import sys, requests, json
import argparse

from oauth_mine import oauth
from oauth_mine import client_id

parser = argparse.ArgumentParser(description="Follows, unfollows, and add and remove notifications for a channel.")
subparsers = parser.add_subparsers(help="Follows, unfollows, add and remove notifs for a channel")
follow_parse = subparsers.add_parser("f", help="Follows a Twitch channel")
follow_parse.add_argument("follow", help="Channel to follow.")
unfollow_parse = subparsers.add_parser("u", help="Unfollows a Twitch channel")
unfollow_parse.add_argument("unfollow", help="Channel to unfollow")
add_notif_parse = subparsers.add_parser("a", help="Adds notification for a Twitch channel")
add_notif_parse.add_argument("add", help="Channel to add notification for.")
remove_notif_parse = subparsers.add_parser("r", help="Removes notification for a Twitch channel")
remove_notif_parse.add_argument("remove", help="Channel to remove notification for.")
args = vars(parser.parse_args())

url = "https://api.twitch.tv/kraken/users/briceorbryce/follows/channels/"

headers = {"Client-ID": client_id,
            "Authorization": oauth,
            "Accept": "application/vnd.twitchtv.v3+json"
          }

if "follow" in args:
  req = requests.put(url + args["follow"], headers=headers)
elif "unfollow" in args:
  print
  print "Are you sure you want to unfollow? [y|n]"
  double_check = raw_input()
  if double_check == "y":
    req = requests.delete(url + args["unfollow"], headers=headers)
  else:
    print "Decided not to unfollow."
    print parser.print_help()
elif "add" in args:
  print "t"
  url += args["add"] + "?notifications=true"
  req = requests.put(url, headers=headers)
elif "remove" in args:
  url += args["remove"] + "?notifications=false"
  req = requests.put(url, headers=headers)
else:
  print parser.print_help()

if req.status_code == 404:
  print "User \"" + sys.argv[2] + "\" does not exist"
  exit()
elif req.status_code == 204:
  print "Successfully unfollowed " + sys.argv[2] + "."
  exit()

print json.dumps(req.json(), indent=2)
