# Twitch Uptime Script

Simple script to follow a Twitch account without having to log in.
It also unfollows a twitch account, and adds and removes notifications for when that account starts a stream.

In order to send Twitch required HTTP headers and methods, the script uses the requests library to build the headers, and the appropriate HTTP methods. 
The HTTP PUT method allows an account to follow another account with or without enabling notifications.
The HTTP DELETE method unfollows an account.

Finally, it checks the HTTP status code Twitch returns and if the Twitch account the user inputed doesn't exist or there was a successful unfollow, the script prints out the json returned from Twitch using json.dumps with an indent of 2.

The file, "oauth_mine" contains a variable called client_id and oauth which are HTTP headers required by Twitch to hit their api. Also, you'll need a Twitch account to generate your client ID as well as an OAuth token.
