# Twitch Uptime Script

Simple script to send one message or multiple messages to a Twitch channel

It uses argparse to determine if the user wants to send one message or multiple messages to a Twitch channel. 

It'll check if the Twitch channel you're sending messages to exists before firing off the message.

The script only connects to the Twitch IRC server for that Twitch channel to send the message and then calls close on the connection. Multiple message sending is also this way; each message opens a new TCP connection.

The username will be hardcoded, since it's usually hardcoded when used normally. Users do not normally change their usernames as well.

The file, "oauth_mine" contains a variable called client_id and oauth which are HTTP headers required by Twitch to hit their api. Also, you'll need a Twitch account to generate your client ID as well as an OAuth token.
