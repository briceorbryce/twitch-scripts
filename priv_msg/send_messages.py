#! /usr/bin/python

import socket, ssl, sys, urllib2
import argparse
from oauth_briceorbryce import oauth
from oauth_briceorbryce import client_id

# TODO
# Username goes here
# it doesn't change every often so best put it in a variable in the code
username = ""

def send(msg, broadcaster):
	if broadcaster == None:
		channel = sys.argv[1]
	else:
		channel = broadcaster
	s = socket.socket()
	s = ssl.wrap_socket(s)
	s.connect (("irc.chat.twitch.tv", 6697))
	s.send("PASS " + oauth + "\r\n")
	s.send("NICK " + username + "\r\n")
	s.send("JOIN #" + channel + "\r\n")
	s.send("PRIVMSG #" + channel + " :" + msg + "\r\n".encode("utf-8"))
	s.close()

def channel_exists(channel):
	url = "https://api.twitch.tv/kraken/channels/" + channel
	req = urllib2.Request(url)
	req.add_header("Client-ID", client_id)
	req.add_header("Accept", "application/vnd.twitchtv.v3+json")
	try:
		response = urllib2.urlopen(req)
		return True
	except urllib2.HTTPError as e:
		print e
		return False
parser = argparse.ArgumentParser(description="Either send one msg to a Twitch channel or multiple msgs.")
subparsers = parser.add_subparsers(help="Send one command or multiple")

one_cmd_parse = subparsers.add_parser("m", help="Twitch channel to send 1 message to.")
one_cmd_parse.add_argument("channel", help="Twitch channel.")
one_cmd_parse.add_argument("message", help="Message to send.", nargs="+")

multi_cmd_parse = subparsers.add_parser("c", help="Continuously send multiple messages to a Twitch Channel.")
multi_cmd_parse.add_argument("channel", help="Send messages continuously.")

#print args args get sent to Namespace
args = vars(parser.parse_args())

channel = args["channel"]
if not channel_exists(channel):
	print "Channel doesn\'t exist.\nExiting"
	exit()
# If messages is in the Namespace tuple, then send 1 msg to a channel
if "message" in args:
	msg = ""
	for msg_loop in args["message"]:
		msg += msg_loop + " "

	send(msg, channel)
else:
	print "Okay, we're continuously sending msgs to " + channel + \
			"\'s channel."
	while True:
		send_mesg = raw_input()
		if send_mesg == "exit":
			print "Exiting continuous message sending"
			exit()
		send(send_mesg, channel)
